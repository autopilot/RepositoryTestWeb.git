﻿using CBLibrary.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RepositoryTestWeb.Entity
{
    public class OrderEntity : EntityIntIdWithCreationModificationAudited
    {
        public string OrderId { get; set; }

        public decimal Amount { get; set; }
    }

    public class OrderConfiguration : IEntityTypeConfiguration<OrderEntity>
    {
        public void Configure(EntityTypeBuilder<OrderEntity> builder)
        {
            //用户表
            builder.ToTable("Orders");
            //用户名 唯一索引
            builder.HasIndex(o => o.OrderId).IsUnique();
        }
    }
}
