﻿using CBLibrary.Repository.Application;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Order
{
    public interface IOrderService : IAppServiceCreationModificationAudited<int, OrderEntity, OrderDto>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        Task<ResultMessage<OrderDto>> GetByOrderIdAsync(string orderId);
    }
}
