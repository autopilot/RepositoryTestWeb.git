﻿using CBLibrary.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RepositoryTestWeb.Entity
{
    public class GoodsEntity : EntityIntIdWithSoftDeletionAudited
    {
        public string GoodsName { get; set; }
    }

    public class GoodsConfiguration : IEntityTypeConfiguration<GoodsEntity>
    {
        public void Configure(EntityTypeBuilder<GoodsEntity> builder)
        {
            //日志表
            builder.ToTable("Goods");
            //名称 唯一索引
            builder.HasIndex(u => u.GoodsName).IsUnique();
        }
    }
}
