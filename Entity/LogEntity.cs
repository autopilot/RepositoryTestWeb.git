﻿using CBLibrary.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace RepositoryTestWeb.Entity
{
    public class LogEntity : EntityIntId
    {
        public DateTime LogTime { get; set; }
        public string Message { get; set; }
    }

    public class LogConfiguration : IEntityTypeConfiguration<LogEntity>
    {
        public void Configure(EntityTypeBuilder<LogEntity> builder)
        {
            //日志表
            builder.ToTable("Logs");
        }
    }
}
