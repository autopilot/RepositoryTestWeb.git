﻿using Microsoft.AspNetCore.Mvc;
using RepositoryTestWeb.AppService.Order;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace RepositoryTestWeb.Controllers
{
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            this._orderService = orderService;
        }


        // GET: api/Order
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            var logs = await this._orderService.GetAllAsync();
            var logsFromCache = await this._orderService.GetAllFromCacheAsync();

            return Json(logs);
        }

        //// GET: api/Order/5
        //[HttpGet("{orderId}", Name = "Get")]
        //public async Task<IActionResult> Get(string orderId)
        //{
        //    var result = await this._orderService.GetByOrderIdAsync(orderId);

        //    return Json(result);
        //}

        // POST: api/Order
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] OrderDto input)
        {
            input.OrderId = Guid.NewGuid().ToString("N");
            var result = await this._orderService.AddAsync(input);

            return Json(result);
        }

        // PUT: api/Order/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
