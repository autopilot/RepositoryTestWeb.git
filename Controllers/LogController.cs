﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RepositoryTestWeb.AppService.Log;
using System;
using System.Threading.Tasks;

namespace RepositoryTestWeb.Controllers
{
    public class LogController : ControllerBase
    {
        private readonly ILogService _logService;

        public LogController(ILogService logService)
        {
            this._logService = logService;
        }


        // GET: api/Log
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            var logs = await this._logService.GetAllAsync();
            var logsFromCache = await this._logService.GetAllFromCacheAsync();

            return Json(logs);
        }

        // GET: api/Log/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Log
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] LogDto input)
        {
            var result = await this._logService.AddAsync(input);

            return Json(result);
        }

        // PUT api/Log
        [HttpPut]
        [AllowAnonymous]
        public async Task<IActionResult> Put([FromBody]LogDto input)
        {
            input.LogTime = DateTime.Now;
            var result = await this._logService.UpdateAsync(input);

            return Json(result);
        }

        // PUT: api/Log/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Delete(int id)
        {
            var dto = await this._logService.GetByIdAsync(id);
            var result = await this._logService.DeleteAsync(dto);

            return Json(result);
        }
    }
}
