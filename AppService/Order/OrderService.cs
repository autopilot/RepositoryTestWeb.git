﻿using AutoMapper;
using CBLibrary.Repository.Application;
using CBLibrary.Repository.Cache;
using CBLibrary.Repository.Dto;
using CBLibrary.Repository.Extensions;
using CBLibrary.Repository.Repository;
using CBLibrary.Repository.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RepositoryTestWeb.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Order
{
    /// <summary>
    /// 添加审计+修改审计
    /// </summary>
    public class OrderService : AppServiceCreationModificationAudited<OrderService, int, OrderEntity, OrderDto>, IOrderService
    {
        /// <summary>
        /// 缓存key,为空不启用缓存
        /// </summary>
        protected override string Cachekey { get; set; } = "Order:All";

        public OrderService(
            ILogger<OrderService> logger,
            IMapper mapper,
            IHttpContextAccessor accessor,
            IUnitOfWork unitOfWork,
            ICacheService cacheService,
            IRepositoryWithIntIdCreationModificationAudited<OrderEntity> repository) : base(logger, mapper, accessor, unitOfWork, cacheService, repository)
        {

        }


        public async Task<ResultMessage<OrderDto>> GetByOrderIdAsync(string orderId)
        {
            var dto = (await GetByPredicateFromCacheAsync(p => p.OrderId.EqualsWithIgnoreCase(orderId.Trim()))).FirstOrDefault();

            return new ResultMessage<OrderDto>(dto);
        }
    }

}
