﻿using AutoMapper.Configuration;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System;

namespace RepositoryTestWeb.AppService.Log
{
    public class LogDto : DtoIntId
    {
        public DateTime LogTime { get; set; }
        public string Message { get; set; }

        public override void CreateMap(MapperConfigurationExpression expression)
        {
            expression.CreateMap<LogEntity, LogDto>();

            expression.CreateMap<LogDto, LogEntity>()
                .ForMember(dest => dest.LogTime,
                    opt => opt.Condition(src => src.LogTime != DateTime.MinValue));
        }
    }
}
