﻿using AutoMapper;
using CBLibrary.Repository.Application;
using CBLibrary.Repository.Cache;
using CBLibrary.Repository.Dto;
using CBLibrary.Repository.Repository;
using CBLibrary.Repository.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RepositoryTestWeb.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Log
{
    /// <summary>
    /// 不启用审计日志 
    /// </summary>
    public class LogService : AppServiceBase<LogService, int, LogEntity, LogDto>, ILogService
    {
        /// <summary>
        /// 缓存key,为空不启用缓存
        /// </summary>
        protected override string Cachekey { get; set; }

        public LogService(
            ILogger<LogService> logger,
            IMapper mapper,
            IHttpContextAccessor accessor,
            IUnitOfWork unitOfWork,
            ICacheService cacheService,
            IRepositoryWithIntId<LogEntity> repository) : base(logger, mapper, accessor, unitOfWork, cacheService, repository)
        {

        }


        public async Task<ResultMessage<LogDto>> GetByMessageAsync(string message)
        {
            var dto = (await GetByPredicateAsync(p => p.Message.Contains(message.Trim()))).FirstOrDefault();

            return new ResultMessage<LogDto>(dto);
        }
    }
}
