﻿using AutoMapper.Configuration;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;

namespace RepositoryTestWeb.AppService.Goods
{
    public class GoodsDto : DtoIntIdWithSoftDeletionAudited
    {
        public string GoodsName { get; set; }


        public override void CreateMap(MapperConfigurationExpression expression)
        {
            expression.CreateMap<GoodsEntity, GoodsDto>();

            expression.CreateMap<GoodsDto, GoodsEntity>();
        }
    }
}
