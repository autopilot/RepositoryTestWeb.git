﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RepositoryTestWeb.AppService.Goods;
using System.Threading.Tasks;

namespace RepositoryTestWeb.Controllers
{
    public class GoodsController : ControllerBase
    {
        private readonly IGoodsService _goodsService;

        public GoodsController(IGoodsService goodsService)
        {
            this._goodsService = goodsService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            var dtos = await this._goodsService.GetAllAsync();

            return Json(dtos);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] GoodsDto input)
        {
            var result = await this._goodsService.AddAsync(input);

            return Json(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id)
        {
            var dto = await this._goodsService.GetByIdAsync(id, false);
            var result = await this._goodsService.SoftDeleteAsync(dto);

            return Json(result);
        }

        [HttpDelete("{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Delete(int id)
        {
            var dto = await this._goodsService.GetByIdAsync(id, false);
            var result = await this._goodsService.DeleteAsync(dto);

            return Json(result);
        }
    }
}
