﻿using AutoMapper.Configuration;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System;

namespace RepositoryTestWeb.AppService
{
    public class UserDto : DtoGuidWithFullAudited
    {
        public string UserName { get; set; }
        public string Password { get; set; }


        public override void CreateMap(MapperConfigurationExpression expression)
        {
            expression.CreateMap<UserEntity, UserDto>();

            expression.CreateMap<UserDto, UserEntity>()
                .ForMember(dest => dest.CreationTime, opt => opt.Condition(src => src.CreationTime != DateTime.MinValue));
        }
    }
}
