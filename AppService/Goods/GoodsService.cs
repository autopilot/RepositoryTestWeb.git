﻿using AutoMapper;
using CBLibrary.Repository.Application;
using CBLibrary.Repository.Cache;
using CBLibrary.Repository.Dto;
using CBLibrary.Repository.Repository;
using CBLibrary.Repository.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RepositoryTestWeb.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Goods
{
    public class GoodsService : AppServiceSoftDeletionAudited<GoodsService, int, GoodsEntity, GoodsDto>, IGoodsService
    {
        protected override string Cachekey { get; set; }

        public GoodsService(
            ILogger<GoodsService> logger,
            IMapper mapper,
            IHttpContextAccessor accessor,
            IUnitOfWork unitOfWork,
            ICacheService cacheService,
            IRepositoryWithIntId<GoodsEntity> repository) : base(logger, mapper, accessor, unitOfWork, cacheService, repository)
        {

        }


        public async Task<ResultMessage<GoodsDto>> GetByNameAsync(string name)
        {
            var dto = (await GetByPredicateAsync(p => p.GoodsName.Contains(name.Trim()), false)).FirstOrDefault();

            return new ResultMessage<GoodsDto>(dto);
        }
    }
}
