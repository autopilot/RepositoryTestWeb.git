﻿using CBLibrary.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RepositoryTestWeb.Entity
{
    public class UserEntity : EntityGuidWithFullAudited
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            //用户表
            builder.ToTable("Users");
            //用户名 唯一索引
            builder.HasIndex(u => u.UserName).IsUnique();
        }
    }
}
