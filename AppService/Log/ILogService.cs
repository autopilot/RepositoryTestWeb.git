﻿using CBLibrary.Repository.Application;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Log
{
    public interface ILogService : IAppServiceBase<int, LogEntity, LogDto>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<ResultMessage<LogDto>> GetByMessageAsync(string message);
    }
}
