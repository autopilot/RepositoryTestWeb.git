﻿using CBLibrary.Repository.Application;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Goods
{
    public interface IGoodsService : IAppServiceSoftDeletionAudited<int, GoodsEntity, GoodsDto>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<ResultMessage<GoodsDto>> GetByNameAsync(string name);
    }
}
