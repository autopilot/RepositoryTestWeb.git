# CBLibrary.Repository.Core Demo

## 数据库
### 创建数据库(mysql为例)
```
CREATE DATABASE IF NOT EXISTS RepositoryTestDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
```

### 生成迁移脚本
```
dotnet ef migrations add Init
// dotnet ef database update(设置了自动迁移的不用执行)
```