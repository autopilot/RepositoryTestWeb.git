﻿using CBLibrary.JwtBearer.Model;
using CBLibrary.Repository.Application;
using RepositoryTestWeb.Entity;
using System;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService
{
    public interface IUserService : IAppServiceFullAudited<Guid, UserEntity, UserDto>
    {
        //Task<ResultMessage> AddAsync(UserDto input);

        //ResultMessage<IEnumerable<UserDto>> GetAll();

        Task<ResultMessage<UserDto>> GetByUserNameAsync(string userName);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResultMessage<UserDto>> LoginAsync(AuthenticateInput input);
    }
}
