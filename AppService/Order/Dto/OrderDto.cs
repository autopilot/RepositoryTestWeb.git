﻿using AutoMapper.Configuration;
using CBLibrary.Repository.Dto;
using RepositoryTestWeb.Entity;
using System;

namespace RepositoryTestWeb.AppService.Order
{
    public class OrderDto : DtoIntIdWithCreationModificationAudited
    {
        public string OrderId { get; set; }

        public decimal Amount { get; set; }


        public override void CreateMap(MapperConfigurationExpression expression)
        {
            expression.CreateMap<OrderEntity, OrderDto>();

            expression.CreateMap<OrderDto, OrderEntity>()
                .ForMember(dest => dest.CreationTime,
                    opt => opt.Condition(src => src.CreationTime != DateTime.MinValue));
        }
    }
}
