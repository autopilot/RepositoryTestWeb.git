﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace RepositoryTestWeb.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ControllerBase : Controller
    {

    }
}
