﻿using CBLibrary.JwtBearer.IService;
using CBLibrary.JwtBearer.Model;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RepositoryTestWeb.AppService.Authorization
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IUserService _userService;

        public IEnumerable<Claim> Claims { get; set; }

        public AuthorizationService(IUserService userService)
        {
            this._userService = userService;
        }


        public async Task<ResultMessage> AuthenticateAsync(AuthenticateInput input)
        {
            var loginResult = await this._userService.LoginAsync(input);
            if (loginResult.Code >= 0)
            {
                var userDto = loginResult.Data;

                Claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Sid,userDto.Id.ToString()),
                    new Claim(ClaimTypes.Name,userDto.UserName),
                    new Claim(ClaimTypes.Role,"1,2,3,4,5,6")
                };
            }

            return loginResult;
        }
    }
}
